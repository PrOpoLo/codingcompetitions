#include <bits/stdc++.h>
using namespace std;
using namespace chrono;

class Problem
{
public:
	void print();
	int solve();
	int score();
	int greedy();
	int tabuSearch();
	int simulatedAnnealing();
	//Read
	friend istream& operator>> (istream&, Problem&);
	//Write
	friend ostream& operator<< (ostream&, const Problem&);
	vector<array<vector<string>, 2>> vClients_m;
	unordered_map<string, bool> umIngredients_m;
};

string ltrim(const string &);
string rtrim(const string &);
vector<string> split(const string &);

void Problem::print()
{
/*	int noClient_l = 1;
	for (const auto& array_l : vClients_m)
	{
		cout << "Client " << noClient_l << endl;
		cout << "Likes: " << endl;
		for (const auto& ingredient_l : array_l[0])
		{
			cout << ingredient_l << endl;
		}
		cout << "Dislikes: " << endl;
		for (const auto& ingredient_l : array_l[1])
		{
			cout << ingredient_l << endl;
		}
		++noClient_l;
	}
	cout << "\nIngredients:\n";*/

	for (const auto& pair_l : umIngredients_m)
	{
		cout << pair_l.first << " = " << (pair_l.second ? "Chosen" : "Not chosen")<< endl;
	}
}

int Problem::score()
{
	int score_l = 0;

	for (const auto& array_l : vClients_m)
	{
		// Ingrédients disliked
		for (const auto& ingredient_l : array_l[1])
		{
			if (umIngredients_m[ingredient_l] == true)
			{
				goto nextIteration_l;
			}
		}

		// Ingrédients liked
		for (const auto& ingredient_l : array_l[0])
		{
			if (umIngredients_m[ingredient_l] == false)
			{
				goto nextIteration_l;
			}
		}

		++score_l;
		nextIteration_l:;
	}

	return score_l;
}

int Problem::greedy()
{
	int currentScore_l = score();
	for (auto& pair_l : umIngredients_m)
	{
		pair_l.second = false;
		//cout << "Current score is " << currentScore_l << endl;
		//cout << "Removing some " << pair_l.first << endl;
		int score_l = score();
		//cout << "Score would be " << score_l << endl;
		if (score_l > currentScore_l)
		{
			//cout << "Removing ingredient\n";
			currentScore_l = score_l;
		}
		else
		{
			//cout << "Keeping ingredient\n";
			pair_l.second = true;
		}
	}

	return currentScore_l;
}

int Problem::simulatedAnnealing()
{
	int size_l = umIngredients_m.size();
	int indexToInvert_l = 0;
	int currentScore_l = score();
	cout << "Starting score = " << currentScore_l << endl;
	unordered_map<string, bool> umResult_l;
	int swapTimeAllowed_l{1800};
	auto timeLimitTimeStamp_l(steady_clock::now() + seconds(swapTimeAllowed_l));
	int diffScores_l = 0;
	float temperature_l = 100000;
	int nbTrials_l = 0;
	int bestScore_l = currentScore_l;

	while (steady_clock::now() < timeLimitTimeStamp_l)
	{
/*		if (temperature_l < 0.01)
		{
			cout << "temperature tres faible\n";
			break;
		}*/
		indexToInvert_l  = rand() % size_l;

		auto it_l = umIngredients_m.begin();
		advance(it_l, indexToInvert_l);
		it_l->second = !(it_l->second);
		int score_l = score();
		++nbTrials_l;
		// On fait baisser la température par palliers
		if (nbTrials_l % 100 == 0)
		{
			temperature_l *= 0.99;
			//cout << "temperature_l = " << temperature_l << endl;
			//continue;
		}
		if (score_l >= currentScore_l)
		{
			//cout << score_l << " > " << currentScore_l << endl;
			if (score_l > bestScore_l)
			{
				cout << "New best score = " << score_l << endl;
				bestScore_l = score_l;
			}
			umResult_l = umIngredients_m;
		}
		else
		{
			// diffScore_l est donc < 0
			diffScores_l = score_l - currentScore_l;
			//cout << "diffScores_l = " << diffScores_l << endl;
			// On annule, diffScores_l < 0, et exp(diffScores_l / temperature_l) -> 1 quand temperature_l est grand, 0 sinon
			// exp(-(e'-e)/T)
			if (((float) rand() / RAND_MAX) > exp(diffScores_l / temperature_l))
			{
				it_l->second = !(it_l->second);
				//cout << "Worst score and we reject\n";
				continue;
			}
			//cout << "diffScores_l = " << diffScores_l << endl;
			//cout << "Worst score but we accept with probability = " << exp(diffScores_l / temperature_l) << endl;
		}
		currentScore_l = score_l;
	}
	cout << "temperature_l finale = " << temperature_l << endl;
	return bestScore_l;
}

int Problem::tabuSearch()
{
	constexpr int maxTabuSize = 7;
	std::array<int, maxTabuSize> tabuList_l;

	unordered_map<string, bool> umResult_l;
	int size_l = umIngredients_m.size();
	int bestToInvert_l = 0;
	int indexTabuList_l = 0;
	int scoreCandidate_l = 0;
	int bestsCoreCandidate_l = 0;
	int bestScore_l = score();
	cout << "Starting score = " << bestScore_l << endl;
	int swapTimeAllowed_l{600};
	auto timeLimitTimeStamp_l(steady_clock::now() + seconds(swapTimeAllowed_l));
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, size_l - 1);
	int noMove_l = 0;
	//int nbWorse_l = 0;
	while (steady_clock::now() < timeLimitTimeStamp_l)
	{
		// On prend un 1er candidat "au hasard"
		do
		{
			bestToInvert_l = distribution(generator);
		}
		while (std::find(tabuList_l.begin(), tabuList_l.end(), bestToInvert_l) != tabuList_l.end());

		auto it_l = umIngredients_m.begin();
		advance(it_l, bestToInvert_l);
		it_l->second = !(it_l->second);
		scoreCandidate_l = score();
		bestsCoreCandidate_l = scoreCandidate_l;
		it_l->second = !(it_l->second);

/*		if (noMove_l < 20)
		{*/
			// On cherche le meilleur candidat
			for (int i = 0; i < size_l; ++i)
			{
/*				if (i < 0)
					continue;
				if (i == size_l)
					break;*/
				// i dans liste taboue, on zappe
				if (std::find(tabuList_l.begin(), tabuList_l.end(), i) != tabuList_l.end())
					continue;
				auto it_l = umIngredients_m.begin();
				advance(it_l, i);
				it_l->second = !(it_l->second);
				scoreCandidate_l = score();
				if (scoreCandidate_l > bestsCoreCandidate_l)
				{
					bestToInvert_l = i;
					bestsCoreCandidate_l = scoreCandidate_l;
				}
				it_l->second = !(it_l->second);
			}
/*		}
		else
		{
			++nbWorse_l;
			if (nbWorse_l == maxTabuSize)
			{
				noMove_l = 0;
				nbWorse_l = 0;
			}
			// On cherche le pire candidat pour faire bcp bouger la solution
			for (int i = 0; i < size_l; ++i)
			{
				// i dans liste taboue, on zappe
				if (std::find(tabuList_l.begin(), tabuList_l.end(), i) != tabuList_l.end())
					continue;
				auto it_l = umIngredients_m.begin();
				advance(it_l, i);
				it_l->second = !(it_l->second);
				scoreCandidate_l = score();
				if (scoreCandidate_l < bestsCoreCandidate_l)
				{
					bestToInvert_l = i;
					bestsCoreCandidate_l = scoreCandidate_l;
				}
				it_l->second = !(it_l->second);
			}
		}*/

		it_l = umIngredients_m.begin();
		advance(it_l, bestToInvert_l);
		it_l->second = !(it_l->second);
		//cout << "Trying to invert " << it_l->first << ", old = " << (it_l->second ? "true" : "false") << ", new = " << (it_l->second ? "false" : "true") << endl;

		//cout << "Test score = " << score_l << endl;
		if (bestsCoreCandidate_l > bestScore_l)
		{
			noMove_l = 0;
			cout << "New best score = " << bestsCoreCandidate_l << endl;
			bestScore_l = bestsCoreCandidate_l;
			umResult_l = umIngredients_m;
		}
		else
		{
			//cout << "worst or equal score" << endl;
			//it_l->second = !(it_l->second);
			++noMove_l;
		}
		tabuList_l[indexTabuList_l % maxTabuSize] = bestToInvert_l;
		++indexTabuList_l;
/*		cout << "TabuList = \n";
		for (auto i : tabuList_l)
		{
			cout << i << endl;
		}*/
	}

	umIngredients_m = umResult_l;
	return bestScore_l;
}

int Problem::solve()
{
	int score_l = greedy();
	//unordered_map<string, bool> umResult_l = umIngredients_m;
	//score_l = tabuSearch();
	//cout << "Score with tabuSearch: " << score_l << endl;
	//umIngredients_m = umResult_l;
	score_l = simulatedAnnealing();
	cout << "Score with simulatedAnnealing: " << score_l << endl;
	//print();
	return score_l;
}

//Reader from input file
istream& operator>>(istream& is_p, Problem& problem_p)
{
	int nbClients_l = 0;

	is_p >> nbClients_l;
	//cout << "nbClients_l = " << nbClients_l << endl;
	problem_p.vClients_m = vector<array<std::vector<string>, 2>>(nbClients_l, {vector<string>{}, vector<string>{}});
	problem_p.vClients_m.reserve(nbClients_l);
	//std::hash<std::string> hasher_l;

	for (int i = 0; i < nbClients_l; ++i)
	{
		for (int likeDislike_l = 0; likeDislike_l <= 1; ++likeDislike_l)
		{
			int nbIngredients_l;
			is_p >> nbIngredients_l;
			//cout << "nbIngredients_l for client " << (i+1) << " = " << nbIngredients_l << endl;
			problem_p.vClients_m[i][likeDislike_l].reserve(nbIngredients_l);
			string ingredient_l{};
			for (int j = 0; j < nbIngredients_l; ++j)
			{
				is_p >> ingredient_l;
				//cout << "ingredient_l = " << ingredient_l << endl;
				//auto hashed_l = hasher_l(ingredient_l);
				problem_p.vClients_m[i][likeDislike_l].push_back(ingredient_l);
				problem_p.umIngredients_m[ingredient_l] = true;
			}
			//sort(vClients_m[i][likeDislike_l].begin(), vClients_m[i][likeDislike_l].end(), std::less<int>());
		}
	}

	return is_p;
}

//Writer in output file
ostream& operator<<(ostream& os_p, const Problem& problem_p)
{
	string toDump_l;
	int total_l = 0;
	for (const auto& pair_l : problem_p.umIngredients_m)
	{
		if (pair_l.second)
		{
			toDump_l += " " + pair_l.first;
			++total_l;
		}
	}

	os_p << total_l << toDump_l << endl;

	return os_p;
}

void readInput(const string& fileName_p,Problem& pb_p)
{
	//Open stream
	ifstream myFile_l;
	myFile_l.open(fileName_p.c_str());
	//Read
	myFile_l >> pb_p;
	//Close file
	myFile_l.close();
}

void writeOutput(const string& inputFilename_p, const Problem& pb_p, int eval_p)
{
	//Open stream
	ofstream myFile_l;
	string inputFilename_l = inputFilename_p.substr(0,inputFilename_p.find("."));
	string fileName_l = "out/" + inputFilename_l.substr(3,inputFilename_l.size()) + "_" + to_string(eval_p) + ".out";
	myFile_l.open(fileName_l.c_str());
	// Write
	myFile_l << pb_p;
	//Close file
	myFile_l.close();
}

int main(int argc, char** argv)
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);

	for (int i = 1; i < argc; ++i)
	{
		Problem problem_l;
		readInput(argv[i], problem_l);
		writeOutput(argv[i], problem_l, problem_l.solve());
	}

}

string ltrim(const string &str) {
    string s(str);

    s.erase(
        s.begin(),
        find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
        find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
        s.end()
    );

    return s;
}

vector<string> split(const string &str) {
    vector<string> tokens;

    string::size_type start = 0;
    string::size_type end = 0;

    while ((end = str.find(" ", start)) != string::npos) {
        tokens.push_back(str.substr(start, end - start));

        start = end + 1;
    }

    tokens.push_back(str.substr(start));

    return tokens;
}
