#include <bits/stdc++.h>
using namespace std;

class Problem
{
public:
	void print();
	int eval();
	int solve();
	//Read
	friend istream& operator>> (istream&, Problem&);
	//Write
	friend ostream& operator<< (ostream&, const Problem&);
	// add member variables here
};

void Problem::print()
{
}

int Problem::eval()
{
	int score_l = 0;

	return score_l;
}

int Problem::solve()
{
	int score_l = 0;

	return score_l;
}

//Reader from input file
istream& operator>>(istream& is_p, Problem& problem_p)
{
	// Reading from is_p
	//is_p >>

	return is_p;
}

//Writer in output file
ostream& operator<<(ostream& os_p, const Problem& problem_p)
{
	// Writing to os_p
	//os_p <<

	return os_p;
}

void readInput(const string& fileName_p,Problem& pb_p)
{
	//Open stream
	ifstream myFile_l;
	myFile_l.open(fileName_p.c_str());
	//Read
	myFile_l >> pb_p;
	//Close file
	myFile_l.close();
}

void writeOutput(const string& inputFilename_p, const Problem& pb_p, int eval_p)
{
	//Open stream
	ofstream myFile_l;
	string inputFilename_l = inputFilename_p.substr(0, inputFilename_p.find("."));
	string fileName_l = "out/" + inputFilename_l.substr(3, inputFilename_l.size()) + "_" + to_string(eval_p) + ".out";
	myFile_l.open(fileName_l.c_str());
	// Write
	myFile_l << pb_p;
	//Close file
	myFile_l.close();
}

int main(int argc, char** argv)
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);

	for (int i = 1; i < argc; ++i)
	{
		Problem problem_l;
		readInput(argv[i], problem_l);
		writeOutput(argv[i], problem_l, problem_l.solve());
	}

}
